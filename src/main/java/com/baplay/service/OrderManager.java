package com.baplay.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baplay.dao.OrderDao;
import com.baplay.dto.Order;
import com.baplay.utils.HttpUtil;

@Service
@Transactional
public class OrderManager {
	//http://rcshow.tv/cron/export_data.php?start=20160301&end=20160310
	private static final String url = "http://rcshow.tv/cron/export_data.php?start=%s&end=%s";
		
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	
	private static final SimpleDateFormat sdfT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	private OrderDao orderDao;
	
	public void saveCk101FromJson(Date startDate, Date endDate) {
		String start = sdf.format(startDate);
		String end = sdf.format(endDate);
		String jsons = HttpUtil.sendGet(String.format(url, start, end));
		System.out.println("jsons1="+jsons);
		if (StringUtils.isBlank(jsons)) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsons = HttpUtil.sendGet(String.format(url, start, end));
		}
		System.out.println("jsons2="+jsons);
		JSONArray jsonArray = JSONArray.parseArray(jsons);
		for (int i=0; i<jsonArray.size(); i++) {
			JSONObject json = jsonArray.getJSONObject(i);
			Order order = new Order();
			order.setOrderType("ck101");
			order.setOrderId(json.getString("oid"));
			order.setUid(json.getString("uid"));
			try {
				order.setMoney(json.getIntValue("amount"));
				order.setOrderTime(new Timestamp(sdfT.parse(json.getString("dtime")).getTime()));
				orderDao.add(order);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Order add(Order order) {
		return orderDao.add(order);
	}

}
