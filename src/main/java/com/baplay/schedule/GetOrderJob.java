package com.baplay.schedule;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.baplay.service.OrderManager;



@Component 
public class GetOrderJob {
	
		
	private static Logger logger = LoggerFactory.getLogger(GetOrderJob.class);
	
	
	@Autowired
	protected OrderManager orderManager;
	/**
	 * 每天凌晨抓前一天推廣兌換單
	 */
	@Scheduled(cron="00 01 00 * * ?")
	public void getOrder() {
		logger.info("============= getOrder start ================");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		orderManager.saveCk101FromJson(cal.getTime(), cal.getTime());
		logger.info("============= getOrder end ================");
	}
	
	
}