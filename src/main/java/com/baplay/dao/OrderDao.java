package com.baplay.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.baplay.dto.Order;

@Repository
public class OrderDao extends BaseDao{
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public OrderDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public Order add(Order order) {
		final String sql = 
				"INSERT INTO `rcorder` (`order_type`, `order_id`, `uid`, `money`, `order_time`) VALUES "
				+ "(?, ?, ?, ?, ?)";
		
		return (Order) addForObject(dsMnMainUpd, sql, order, new Object[] { 
				order.getOrderType(), order.getOrderId(), order.getUid(), order.getMoney(), order.getOrderTime()});
	}

}
