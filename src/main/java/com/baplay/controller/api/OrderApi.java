package com.baplay.controller.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.constants.Config;
import com.baplay.controller.BaseController;
import com.baplay.service.OrderManager;
import com.baplay.utils.MD5Util;

@Controller
@Scope("request")
public class OrderApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(OrderApi.class);
	
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	
	@Autowired
	protected OrderManager orderManager;
	
	/**
	 * 推廣
	 */
	@RequestMapping(value = "/getOrder")
	public @ResponseBody Map<String, String> order(
			@RequestParam("start") String start, 
			@RequestParam("end") String end) {
		Map<String, String> result = new HashMap<String, String>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== OrderApi getOrder ====================\n");
		sf.append("input param [start=").append(start).append(", end=").append(end).append("]"); 
		
		LOG.info(sf.toString());
		try {
			Date startDate = sdf.parse(start);
			Date endDate = sdf.parse(end);
			orderManager.saveCk101FromJson(startDate, endDate);
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(MD5Util.crypt("rcpay"));
	}
}
